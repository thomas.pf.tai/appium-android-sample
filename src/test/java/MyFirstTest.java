import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

import static java.lang.Thread.sleep;

public class MyFirstTest {

    public AppiumDriver<MobileElement> driver;
    public WebDriverWait wait;

    //Elements
    String textViewItemHK = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.TextView";
    String textEditFirstName = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.EditText";
    String textEditLastName = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.EditText";
    String textEditEmail = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[3]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.EditText";
    String textEditPassword = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[4]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.EditText";
    String viewGroupStartwithProfile = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup";
    String textViewLiveIn = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.TextView";
    String textViewItemLiveInHK = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView";
    String textViewWorkPermit = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.TextView";
    String textViewItemHKResidence = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.ListView/android.widget.LinearLayout[1]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView";
    String textViewHighestEdu = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout[3]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.TextView";
    String textViewItemUGrad = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView";
    String textViewYearOfExp = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout[4]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.TextView";
    String layoutStandardPrivacy = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.RelativeLayout";
    String viewGroupSearchJob = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup";

    long TICKS_AT_EPOCH = 621355968000000000L;
    long tick = System.currentTimeMillis()*10000 + TICKS_AT_EPOCH;
//    String imageView = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.widget.ImageView";

    @BeforeMethod
    public void setup () throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("deviceName", "Pixel 2 API 26");
        caps.setCapability("udid", "emulator-5554"); //DeviceId from "adb devices" command
        caps.setCapability("platformName", "Android");
        caps.setCapability("platformVersion", "8.0");
        caps.setCapability("skipUnlock","true");
        caps.setCapability("appPackage", "com.jobsdb");
        caps.setCapability("appActivity","com.jobsdb.SplashActivity");
        caps.setCapability("noReset","false");
        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),caps);
        wait = new WebDriverWait(driver, 10);
    }


    @Test
    public void basicTest () throws InterruptedException {
        //In Country Selector, choose Hong Kong - English and tap Continue
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.jobsdb:id/textView"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textViewItemHK))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.jobsdb:id/continue_button"))).click();

        //Register
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.jobsdb:id/get_start_button"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textEditFirstName))).sendKeys("Amanda");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textEditLastName))).sendKeys("Hugginkiss");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textEditEmail))).sendKeys("amanda_" + tick + "@example.org");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textEditPassword))).sendKeys("123456");
        driver.hideKeyboard();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.jobsdb:id/register_button"))).click();

        //Hit Back button to bypass add to Google dialogue
        sleep(3000);
        driver.navigate().back();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(viewGroupStartwithProfile))).click();

        //Fill in profile
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textViewLiveIn))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textViewItemLiveInHK))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textViewWorkPermit))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textViewItemHKResidence))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textViewHighestEdu))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textViewItemUGrad))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textViewYearOfExp))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.jobsdb:id/done_btn"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.jobsdb:id/next_button"))).click();

        //Choose Profile Privacy
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(layoutStandardPrivacy))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.jobsdb:id/continue_button"))).click();

        //Make a search
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(viewGroupSearchJob))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.jobsdb:id/autoComplete"))).sendKeys("Green Tomato");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.jobsdb:id/long_search_button"))).click();

        //Check if there is result
        wait.until(ExpectedConditions.textMatches(By.id("com.jobsdb:id/search_info"), Pattern.compile("^\\d+ job[s]? for Green Tomato$")));
    }

    @AfterMethod
    public void teardown(){
        driver.quit();
    }

}