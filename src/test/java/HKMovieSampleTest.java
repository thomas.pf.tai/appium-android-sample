import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

import static java.lang.Thread.sleep;

public class HKMovieSampleTest {
    public AppiumDriver<MobileElement> driver;
    public WebDriverWait wait;

    String interstitialCloseButton = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ImageButton";
    String bannerShareButton = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ImageView[3]";
    String bannerShareWithMessageButton = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.ListView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.FrameLayout/android.widget.ImageView";
    String bannerCloseButton = "hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ImageView[4]";

    @BeforeMethod
    public void setup () throws MalformedURLException {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("deviceName", "Pixel 2 API 26");
        caps.setCapability("udid", "emulator-5554"); //DeviceId from "adb devices" command
        caps.setCapability("platformName", "Android");
        caps.setCapability("platformVersion", "8.0");
        caps.setCapability("skipUnlock","true");
        caps.setCapability("appPackage", "gt.farm.hkmovies");
        caps.setCapability("appActivity","gt.farm.hkmovie.RoutingActivity");

        caps.setCapability("noReset","false");
        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),caps);
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void basicTest () throws InterruptedException {
        //Click the language selector
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("android:id/button1"))).click();

        try {
            //close the first interstitial
            MobileElement firstInterstitial = (MobileElement) wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(interstitialCloseButton)));
            firstInterstitial.click();
        } catch (TimeoutException ex) {
            System.out.print("Interstitial not found, continue");
        }

        //Finish tutorial and go to landing page
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gt.farm.hkmovies:id/btnNext"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gt.farm.hkmovies:id/btnNext"))).click();

        //change item layout from table to list
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gt.farm.hkmovies:id/search_bar_mic_or_ex"))).click();

        //wait for banner to show up and tap it
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("link"))).click();

        try {
            //try to share with message and cancel operator
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(bannerShareButton))).click();
            driver.navigate().back();
//            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(bannerShareWithMessageButton))).click();
//            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("android:id/button2"))).click();
        } catch (TimeoutException ex) {
            //The ad did not call up Hotmob internal browser, just tap Back then
            driver.navigate().back();
        }

        try {
            //Close the 2nd interstitial
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(interstitialCloseButton))).click();
        } catch (TimeoutException ex) {
            System.out.print("Interstitial not found, continue");
        }

        //Change item layout to list, wait for banner to show and click it
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gt.farm.hkmovies:id/search_bar_mic_or_ex"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gt.farm.hkmovies:id/search_bar_mic_or_ex"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("link"))).click();

        try {
            //Close the banner
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(bannerCloseButton))).click();
        } catch (TimeoutException ex) {
            //The ad did not call up Hotmob internal browser, just tap Back then
            driver.navigate().back();
        }
    }

    @AfterMethod
    public void teardown(){
        driver.quit();
    }
}
